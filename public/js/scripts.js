$.extend({
		  getUrlVars: function(){
			var vars = [], hash;
			var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
			for(var i = 0; i < hashes.length; i++)
			{
			  hash = hashes[i].split('=');
			  vars.push(hash[0]);
			  vars[hash[0]] = hash[1];
			}
			return vars;
		  },
		  getUrlVar: function(name){
			return $.getUrlVars()[name];
		  }
});

var itp = {
	init : function(){
		$(document).ready(function(){
			$('.arrow-down-icon').on('click', function(){
				$('profile-dropdown').toggleClass('shown');
			});
			var allVars = $.getUrlVars();
			$.ajax({url: "/api/department/" + allVars.email , 
				error: function(xhr){
					console.log("An error occured: " + xhr.status + " " + xhr.statusText);
				},
				success: function(result){
					console.log(result);
				}
			});
		});
	}
}