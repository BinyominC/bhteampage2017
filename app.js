var MongoClient = require('mongodb').MongoClient;
var tools = require('./tools');
var url = tools.databaseURL;

const express = require('express');
const app = express();
app.use(express.static('public'));

app.get('/api/departmentList', function (req, res) {
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var query = {};
      db.collection("employees").distinct("department", function(err, result) {
        if (err) throw err;
        console.log(result);
        res.json(result.sort());
        db.close();
      });
    });
    
});
app.get('/api/department/:departmentName', function (req, res) {

    getDepartmentByDepartmentName(req, res);
    
});

app.get('/api/department/:email', function (req, res) {
  getDepartmentByEmail(req, res);

    // MongoClient.connect(url, function(err, db) {
    //   if (err) throw err;
    //   var query = {};
    //   var _email = req.params.email;
    //   db.collection("employees").find({email: _email}).toArray(function(err, result) {
    //     if (err) throw err;
    //     console.log(result);
    //     res.json(result);
    //     db.close();
    //   });
    // });
    
});

app.get('/api/department/:username', function (req, res) {
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var query = {};
      var _username = req.params.username;
	  db.collection("employees").find({email: {$regex : _username+"@*" }}).toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        res.json(result);
        db.close();
      });
    });
});
app.get('/api/employee/:employeeEmail', function (req, res) {
  getEmployeeByEmail(req, res);

    // MongoClient.connect(url, function(err, db) {
    //   if (err) throw err;
    //   var _email = req.params.employeeEmail;
    //   db.collection("employees").find({email: _email}).toArray(function(err, result) {
    //     if (err) throw err;
    //     console.log(result);
    //     res.json(result);
    //     db.close();
    //   });
    // });
    
});

function getDepartmentByEmail(req, res) {
  getEmployeeByEmail(req, res, getDepartmentByDepartmentName(req, res, departmentName))
}

function getEmployeeByEmail(req, res, cb){

  MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var _email = req.params.employeeEmail;
      db.collection("employees").find({email: _email}).toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        if(cb) {
          db.close();
          cb(req, res, result.departmentName);
        } else {
          res.json(result);
          db.close();
        }
      });
    });
}

function getDepartmentByDepartmentName(req, res, departmentName){
  MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var query = {};
      var _department = departmentName || req.params.departmentName;
      db.collection("employees").find({department: _department}).toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        res.json(result);
        db.close();
      });
    });
}

app.get('/teams/:teamId/firstName/:firstName', function (req, res) {
  res.send(req.params)
});
app.listen(3000, function () {
  console.log('app listening on port 3000!')
});



