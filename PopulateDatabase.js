var fs = require('fs');
var MongoClient = require('mongodb').MongoClient;
var tools = require('./tools');
var url = tools.databaseURL;

fs.readFile('.\\bhteampage2017\\phonelist.txt', 'utf8', function(err, contents) {
    var contentsArray = contents.toString().split('\n');
	addContentsToDatabase(contentsArray);
	
});

function addContentsToDatabase(contentsArray){
	var employees=[];
	for (var i = 0; i < contentsArray.length; i++){
		var personalInfo = contentsArray[i].split(",")
    		var myobj = { 	lastName: personalInfo[0], 
							firstName:  personalInfo[1],
							nickName:  personalInfo[2],
							extension:  personalInfo[3],
							personalExtension:  personalInfo[4],
							pager:  personalInfo[5],
							email:  personalInfo[6],
							department:  personalInfo[7],
							locale:  personalInfo[8],
							floor:  personalInfo[9],
							unknown1:  personalInfo[10],
							userName:  personalInfo[11],
							unknown2:  personalInfo[12],
							unknown3:  personalInfo[13],
							unknown4:  personalInfo[14],
							unknown5:  personalInfo[15],
							unknown6:  personalInfo[16]								  
						};
			if(myobj.department!='LEFT-THE-COMPANY'){
				employees.push(myobj);			
			}
	}	
	
	MongoClient.connect(url, function(err, db) {
		db.collection("employees").insertMany(employees, function(err, res) {
		if (err) throw err;
		console.log("Finished:  "+ employees.length + " documents inserted");
		db.close();
		});
	});		
}








